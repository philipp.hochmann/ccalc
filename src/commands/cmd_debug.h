#pragma once
#include <stdbool.h>

int cmd_debug_check(char *input);
bool cmd_debug_exec(char *input, int code);
