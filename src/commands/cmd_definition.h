#pragma once
#include <stdbool.h>

int cmd_definition_check(char *input);
bool cmd_definition_exec(char *input, int code);
